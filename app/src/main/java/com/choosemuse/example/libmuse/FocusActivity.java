package com.choosemuse.example.libmuse;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class FocusActivity extends Activity{

    private MediaPlayer mediaPlayer;
    private Player mPlayer;

    private String mTags;
    private TextView mSongDisplay1;
    private TextView mSongDisplay2;
    private TextView mSongDisplay3;
    private TextView mSongDisplay4;
    private TextView mSongDisplay5;
    private JSONObject[] mSongQueue;

    private static final int CALIBRATE_REQUEST = 1;
    private static final String MY_PREFS_NAME = "FociData";
    private static final int REQUEST_CODE = 2;
    private static final String CLIENT_ID = "64f2a446445bcbf95cb03f1075e0b4a6";

    private void searchTracks(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://api.soundcloud.com/tracks?client_id="+CLIENT_ID+"&tags='"+mTags+"'";
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            mSongQueue = new JSONObject[response.length()];
                            for(int i =0; i<response.length(); i++){
                                mSongQueue[i] = response.getJSONObject(i);
                            }
                            updateText();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        queue.add(req);
    }

    private void updateText(){
        try {
            mSongDisplay1.setText(mSongQueue[0].getString("title"));
            mSongDisplay2.setText(mSongQueue[1].getString("title"));
            mSongDisplay3.setText(mSongQueue[2].getString("title"));
            mSongDisplay4.setText(mSongQueue[3].getString("title"));
            mSongDisplay5.setText(mSongQueue[4].getString("title"));
            mPlayer.execute("https://api.soundcloud.com/tracks/"+mSongQueue[0].getInt("id")+"/stream?client_id="+CLIENT_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void shiftQueue(){
        for(int i = 0; i< (mSongQueue.length-1); i++){
            mSongQueue[i] = mSongQueue[i+1];
        }
        mSongQueue[mSongQueue.length-1] = null;
        updateText();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_focus);

        mSongDisplay1 = (TextView)findViewById(R.id.song_queue_1);
        mSongDisplay2 = (TextView)findViewById(R.id.song_queue_2);
        mSongDisplay3 = (TextView)findViewById(R.id.song_queue_3);
        mSongDisplay4 = (TextView)findViewById(R.id.song_queue_4);
        mSongDisplay5 = (TextView)findViewById(R.id.song_queue_5);
        mSongDisplay1.setSelected(true);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        mTags = prefs.getString("SongTags","classical,rock,rap,pop,electronic");
        searchTracks();

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer = new Player();
    }

    class Player extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog progress;

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO Auto-generated method stub
            Boolean prepared;
            try {

                mediaPlayer.setDataSource(params[0]);

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        shiftQueue();
                    }
                });
                mediaPlayer.prepare();
                prepared = true;
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                Log.d("IllegalArgument", e.getMessage());
                prepared = false;
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (progress.isShowing()) {
                progress.cancel();
            }
            Log.d("Prepared", "//" + result);
            mediaPlayer.start();
        }

        public Player() {
            progress = new ProgressDialog(FocusActivity.this);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            this.progress.setMessage("Buffering...");
            this.progress.show();

        }
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}
